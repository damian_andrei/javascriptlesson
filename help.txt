/*

Variabile:
=========

	var name = value;
	|	|	 |	 |
	|	|	 |	 |
	|	|	 |	 valoare de orice tip
	|	|	 simbol de asociere
	|	numele sub care stochezi valoarea
	keyword prin care initializezi o variabila

	
	Tipuri de valori si date:
	========================
	
	//valori fixe
	var car; 						//undefined
	var car = 'bmw'; 				//string
	var car = '';					//string - empty text
	var car = null; 				//null
	var car = 2; 					//integer
	var car = 2.2; 					//float
	var car = true; 				//boolean
									
	//liste
	var cars = {					//object (un obiect asociativ format din chei string si valori)
		'brand':'bmw',				//folosire: cars['brand'] returneaza 'bmw'
		'color':'red	
	};
	var cars = [					//array (lista format din chei numerice si valori string)
		'bmw', 						//folosire cars[0] returneaza 'bmw'
		'mercedes'
	];
	var cars = [					//array (lista cu chei numerice si valori formate din alte obiecte)
		{'car':'bmw'},				//folosite: cars[0]['car'] returneaza 'bmw'
		{'car':'mercedes'}
	];
	
	//valori stabilite in alta parte
	var car = her_car; 				//valoarea altei variabile
	var car = getMyCar(); 			//valoarea returnata de o functie



Functii: definitie
=======

		keyword prin care definesti functia
		|		numele prin care va fi apelata functia
		|		|
		|		argumente (nume de variabile separate cu virgula, (ce vor stoca valorile primite in ordinea trimiterii))
		|		|
	function name(arg1, arg2) {
		return valore;
					|
					|
					
	}
	
Functii: invocare
=======
	
	name(arg1, arg2);
	|		|
	|		|
	|		argumente (valori separate cu virgula)
	|		
	numele functiei invocate
*/

Exemplu: 

//definitie
function getMyCar(person){
	if(person == 'Bob') {
		return 'bmw'
	}
	if (person == 'Mary') {
		return 'mini cooper';
	}
}

//invocare
getMyCar('Bob'); //returneaza 'bmw'
