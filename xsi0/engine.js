/*
 * Tic-Tac-Toe
 * Javascript lesson - June 2013
 *
 * Authors: 
 *
 * Alexandru Margineanu <alex.margineanu@gmail.com>
 * Florin Sandu <sandu.florin@gmail.com>
 * Andrei Damian <damianandreimircea81@gmail.com>
 *
 */

 /* TODO: get li collections out of the functions, make a function setMessage(), make a function nextPlayer() */

/******* Game variables */

/* javascript references to HTML tags, found by their id */

	var	notice 			= document.getElementById('notice'), //messages will be displayed here
		board 			= document.getElementById('board'), //<UL> game board
		score 			= { 
						"X"	: document.getElementById('p1score'), //player's X score
						"O"	: document.getElementById('p2score')  //player's O score
		};
		
/* game state */
	
	var	current_player 	= getRandomInt(0,1), //first player is selected randomly from 0 and 1
		game_ended		= false, //game state, it's set to true every time a game ends
		wining_position = [], //winning cell positions found at the end of a game
		state 			= ['.','.','.','.','.','.','.','.','.'], //default game state, all cells are empty
		positions 		= [
						 [0,1,2], //1st row
						 [3,4,5], //2nd row
						 [6,7,8], //3rd row
						 [0,3,6], //1st column
						 [1,4,7], //2nd column
						 [2,5,8], //3rd column
						 [0,4,8], //1st diagonal
						 [2,4,6]  //2nd diagonal
		]

/* strings only */ 

	var players 		= ['X','O'], //player names
		msg_move 		= 'este randul jucatorului ',
		li 				= ''; //used to concatenate all <LI> elements into one string
		


/******* INIT function */	

	/* called from HTML, builds the game for the first time */
	function init() {
		makeBoard();
		notice.innerHTML = 'este randul jucatorului '+players[current_player];
		if(AI == true && current_player == 1){
			AI_Turn();//1st player is AI, let it make the move
		}
	}
	
		
/******* Game functions */	

	/* Generate 9 <LI> cells and attach them to the <UL> game board */
	function makeBoard(){
		for(i=0;i<9;i++){
			li = li + '<li title="'+i+'" id="'+i+'" class="' + isClear(i) + '"></li>';
		}
		score['X'].innerHTML = 0;
		score['O'].innerHTML = 0;
		board.innerHTML=li; /* ataseaza stringul format din <LI>-uri tagului <UL> */
		setClick(); /* invoca functia care seteaza clickul pe fiecare celula */
	}
	
	
	/* Returns true OR false */
	function isWinning(cell){			
		var pattern = ''; /* un string gol */
		for(i=0;i<positions.length;i++){	/* pentru fiecare lista de pozitii  */
			if(contains(positions[i], cell)) { /* se afla celula jucata in lista curenta de pozitii? */
				wining_position = positions[i];		/* salveaza lista pozitiilor incercate pentru mai tarziu */
				for(current_cell=0;current_cell<positions[i].length;current_cell++){ /* pentru fiecare celula vecin gasita in lista */
					
					pattern = pattern + state[positions[i][current_cell]];   /* lipeste valorile celulelor intr-un singur string */
					
					if(pattern == 'XXX' || pattern == 'OOO' ) {	 /* daca textul format este unul din patern-uri */
						game_ended = true;	/* marcheaza sfarsitul reprizei */					
						return true;	/* returneaza ca e o mutare castigatoare */
					}
				}
				pattern = ''; /* goleste stringul la sfarsitul fiecarei iteratii mari*/
			}
		}
		return false; /* nu e mutare castigatoare */
	}
	

	/* Iterate over the winning cells and append a CSS class name */
	function showWinningPosition(){
		for (i=0; i<wining_position.length; i++){
			var cell = document.getElementById(wining_position[i]);
			cell.className = cell.className + ' win';
		}
	}
	
	
	/* Called every time there's a click on a cell */
	function move(cell) { 
		var j = parseInt(cell.id);		
		if(cell.innerHTML=='' && game_ended == false){
			cell.innerHTML=players[current_player];
			state[j]=players[current_player];
			if (isWinning(j)){
				notice.innerHTML = 'Jucatorul ' + players[current_player] + ' a castigat';
				showWinningPosition();
				changeScore();
				document.getElementById('reset').className = '';
			}else{			
				if(current_player==0){
					current_player=1
				}else{
					current_player=0
				}
				notice.innerHTML=msg_move+players[current_player];
			}
		}
		if(gameIsTie()){
			notice.innerHTML = 'Este remiza';
			document.getElementById('reset').className = '';
		}
		
		if (AI == true){ //playing vs AI
			if(AI_turn == true){
				AI_Turn();
				return;//exit current move()
			} else {
				AI_turn = true; //prepare AI for next round
			}
		}
	}
	
	/* Update the score displayed to the user */
	function changeScore(){
		var e = score[players[current_player]];	 //html reference to the score tag for the player that won 
		e.innerHTML = parseInt(e.innerHTML) + 1; //set the score to old score + 1
	}
	
	/* Reset the game board (cells, game state). Called by the user at the end of a game */
	function resetBoard(){
		document.getElementById('reset').className = 'hidden'; //hide reset button
		state = [ '.','.','.','.','.','.','.','.','.' ];
		game_ended = false;
		var cells = document.getElementsByTagName('li');
		for(var i = 0; i < cells.length; i++) {
			var cell = cells[i];
			cell.innerHTML = '';
			if(contains(cell.className, 'clear')){
				cell.className = 'clear';
			}else{
				cell.className = '';
			}
		}
		notice.innerHTML = msg_move+players[current_player];
		console.clear();
	}
	