http://grooveyourself.ro/new/123 -> deschide (salveaza) o sesiune noua sub id-ul 123

returns
{
	"status" : true 
}

http://grooveyourself.ro/save/123/sessiondata[]=0&data[]=1&data[]=0 -> salveaza datale [0,1,0] sub sesiunea cu id-ul 123

returns
{
	"id" : 123,
	"data": [0,1,0],
	"status" : true 
}

http://grooveyourself.ro/get/123/ -> returneaza datele [0,1,0] salvate  sub sesiunea cu id-ul 123

returns
{
	"id" : 123,
	"data": [0,1,0]
}

http://grooveyourself.ro/save/1234/session/data[]=1&data[]=1&data[]=1 -> return false pentru ca sesiunea cu id-ul 1234 nu exista

returns
{
	"status" : false	
}

http://grooveyourself.ro/get/1234/ -> returneaza false pentru ca sesiunea cu id-ul 1234 (nu exista)

returns
{
	"status" : false
}