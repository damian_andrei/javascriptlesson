// Iterate over <LI> tags and call a function on click
function setClick(){
	var cells = document.getElementsByTagName('li');
	for(var i = 0; i < cells.length; i++) {
		var cell = cells[i];
		cell.onclick = function(){move(this)};//send 'this' current cell to move()
	}
}

// Returns true when game is a tie, false when 
// there are empty cells left
function gameIsTie(){
	var cells = document.getElementsByTagName('li');
	for(var i = 0; i < cells.length; i++) {
		var cell = cells[i];
		if(cell.innerHTML == ''){
			return false;
		}
	}
	return true;
}

// Returns clear for 3,6,9,etc...
function isClear(i){
	var is_clear;
	if(i % 3 == 0){
		is_clear='clear';
	}else {
		is_clear='';
	}
	return is_clear;
}

// Returns a random number between 0 (inclusive) and 1 (exclusive)
function getRandom() {
	return Math.random();
}
// Returns a random number between min and max
function getRandomArbitrary(min, max) {
	return Math.random() * (max - min) + min;
}
// Returns a random integer between min and max
// Using Math.round() will give you a non-uniform distribution!
function getRandomInt(min, max) {
	return Math.floor(Math.random() * (max - min + 1)) + min;
}


function contains(haystack, needle){
	if(haystack.indexOf(needle) !== -1) return true;
	else if(haystack.indexOf(needle) == -1) return false;
	else return haystack.indexOf(needle);
}