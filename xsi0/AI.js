/*
 * Tic-Tac-Toe - Computer vs Human
 * Javascript lesson - June 2013
 *
 * Authors: 
 *
 * Alexandru Margineanu <alex.margineanu@gmail.com>
 *
 */
 
var AI 			= true, 
	AI_turn 		= true,
	score_cells 	= [],
	score_series 	= [],
	state_positions 	= [],
	decision;

blank_state(); //initialise the AI with an empty state

function AI_Turn(){

	AI_turn = false; // seteaza mutarea urmatoare pentru player1
	
	var cells = document.getElementsByTagName('li'),
		cell,
		cell_id;
	console.clear();
	
	/*
	1. treci prin toate celulele
		
		2. pentru fiecare celula libera, localizeaza seriile din care face parte celula
			
			3. pentru fiecare serie
				
				4. treci prin fiecare vecin, verifica daca e deja ocupat
				
					-de AI
						5. --daca ambii vecini din serie sunt AI muta in casuta libera (win!)
						   --da nota +5 seriei daca un singur vecin este AI si un vecin este liber
						   
					-e liber
						5. --da seriei nota +1
						   --treci la seria urmatoare (pasul 3)
						   
					-de player1
						5. --anuleaza seria pentru AI cu scor -1
						   --daca ambii vecini sunt player1, strica-i seria
						   --treci la seria urmatoare (pasul 3)
						   
			6. treci prin fiecare scor si muta in casuta cu scorul cel mai mare		
			
			7. resteaza scorurile si asteapta mutarea urmatoare
	*/	

	for(k=0;k<positions.length;k++){//list of positions
		var set = positions[k];
		state_positions[k] = '';
		for(j=0;j<set.length;j++){
			if(state[set[j]] !== '.') state_positions[k] += state[set[j]];
		}
	}
	for(i=0;i<state_positions.length;i++){
		if(state_positions[i] == 'O'){
			for(j=0;j<positions[i].length;j++){
				setTimeout(function(){
				
				console.log(positions[i][j], document.getElementById(positions[i][j]));
				document.getElementById(positions[i][j]).className = document.getElementById(positions[i][j]).className + ' try';}, 100);
				console.log('trying',positions[i][j], cells[positions[i][j]].innerHTML);
				if(cells[positions[i][j]].innerHTML == ''){
					decision = positions[i][j];
					console.log('add second move'+decision);
					break;
				}
			}
		} else if(state_positions[i] == 'OO'){
			console.log('win at'+positions[i]);
			for(j=0;j<positions[i].length;j++){
				console.log('trying',positions[i][j], cells[positions[i][j]].innerHTML);
				if(cells[positions[i][j]].innerHTML == ''){
					decision = positions[i][j];
					break;
				}
			}
		} else if(state_positions[i] == 'XX'){
			for(j=0;j<positions[i].length;j++){
				console.log('trying',positions[i][j], cells[positions[i][j]].innerHTML);
				if(cells[positions[i][j]].innerHTML == ''){
					decision = positions[i][j];
					console.log('break the fucker at'+decision);
					break;
				}
			}
		}
	}
	
	if(!decision) {console.log('first decision');decision = getRandomInt(0,8);}
	console.log('decision',decision);
	blank_state();
	move(document.getElementById(decision));
}

function blank_state(){
	for(i=0;i<9;i++){
		score_cells[i] = 0;
		score_series[i] = 0;
	}
}	